# Addon Shulker personnalisé - Resources pack Enigma - Edition Buildmyworld

![Apercu de l'addon](./preview.jpg)

Version supportée :

- 1.21.0 - 1.21.1

D'autres versions sont supportées disponible sur : https://buildmyworld.fr/resources

## Remerciement

Communauté github du summerfield : [Github Resources pack summerfield](https://github.com/SummerFields/SummerFields)

Lien du topic de l'auteur : [Resources pack enigma](https://www.minecraftforum.net/forums/mapping-and-modding-java-edition/resource-packs/1246204-32x-enigma)

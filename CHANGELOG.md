# Changelog

1.21.0 - 1.21.1 v1.0
-------
- Mise a jour du parametre format pour supporter la 1.21.0 et 1.21.1


1.20.5 - 1.20.6 v1.0
-------
- Mise a jour du parametre format pour supporter la 1.20.5 et 1.20.6


1.20.2 - 1.20.4 v1.0
-------
- Mise a jour du parametre format pour supporter la 1.20.2 a la 1.20.4


1.20 v1.0
-------
- Mise a jour du parametre format pour supporter la 1.20


1.19 v1.0
-------
- Aucun changement


1.18 v1.0
-------
- Création de l'addon pour avoir des shulkers personnalisés servant de caisses utiles à la partie timelapse du projet
